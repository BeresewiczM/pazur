# PAZUR 10
SKN Estymator, Katedra Statystyki UEP, Katedra Metod Matematycznych i Statystycznych UP  
10.04.2015  

## Plan dzisiejszego spotkania

1. Kilka informacji wstępnych
2. Pierwszy referat - Łukasz Smaga, UAM
3. Przerwa (30 minut)
4. Drugi referat - Tomasz Górecki, UAM
5. Część nieoficjalna - pub Fermentownia ;)

## Kilka informacji ze świata R

* **Pogromcy Danych** -- MOOC z analizy danych w pakiecie R (Przemysław Biecek & ICM UW):
  - **Przetwarzanie danych w programie R** (8.04 - 29.04)
    - wprowadzenie do R i RStudio, odczytywanie danych, pętle, funkcje, typy danych i statystyka opisowa, przetwarzanie potokowe danych
  - **Wizualizacja i modelowanie** (6.05 - 27.05)
    - wizualizacja, modelowanie danych jakościowych, modelowanie danych ilościowych
  - obecnie ~ 1800 uczestników szkolenia

* Zapisy na: www.pogromcydanych.icm.edu.pl
* Dyplom po zrobieniu 15/20 zadań

## Kilka informacji ze świata R

* pakiet `readxl` oraz `openxlsx` oparte na `C++` (de facto `RCpp`) umożliwiające import plików xls/xlsx (`openxlsx` jedynie xlsx) -- co najważniejsze, koniec problemów z JAVĄ! (patrz pakiet `XLConnect` czy `gdata`).
* pakiet `sjPlot` dla osób, które chciałby się przesiąść z `SPSS` na `R`.
* RStudio stworzył kilka ciekawych cheatsheet z tworzenia pakietów, shiny czy ggplot2.
* Ukazała się nowa książka Hadleya -- R Packages.
* Revolution Analytics ulepszyło podstawowego R pod nazwą "Revolution R Open", który zdecydowanie przyśpiesza obliczenia macierzeowe.

## Kolejne PAZURy

10 Kwietnia

* Mikołaj Bogucki i Krzysztof Jędrzejewski (Pearson Professional) - Analiza trudności testów sprawdzających znajomość języka angielskiego (Performance analysis of assessment content in an English language learning product):
  - Item Response Theory
  - pakiety `itm`, `irtoys` & `shiny`


## Zaproszenie do wygłaszania referatów!

Jeżeli ktoś z Was jest zainteresowany wygłoszeniem referatu zapraszam do kontaktu!

- Osobiście :)
- Elektronicznie - maciej.beresewicz@ue.poznan.pl lub pazur@konf.ue.poznan.pl

## Czas referatów

18:00 - 18:40 - Wybrane zagadnienia analizy danych funkcjonalnych z wykorzystaniem programu R, Łukasz Smaga, Wydział Matematyki i Informatyki UAM

18:40 - 19:10 - Przewa kawowa

19:10 - 19:50 - Profilowanie w R, Tomasz Górecki, Łukasz Smaga, Wydział Matematyki i Informatyki UAM

20:00 - spotykamy się na przed wejściem do tego budynku :)


## 

Dziękuję za uwagę!


