# PAZUR - pierwsze spotkanie 5 marca 2012r.
PAZUR to Poznański Akademicki Zlot Użytkowników R.

Inauguracyjne spotkanie odbyło się 5 marca 2012r. na Uniwersytecie Ekonomicznym. 

Plan spotkania był następujący (przekopiowane z ogłoszenia):

Wykorzystanie metod statystyki małych obszarów w badaniach ankietowych (Łukasz Wawrowski, SKN Estymator i UStat w Poznaniu). Ocena wiarygodności współczesnych sondaży łączy się z przekonaniem, że dobre szacunki związane są z dużą liczebnością próby. Niestety zwiększenie reprezentatywności nieodłącznie idzie w parze ze wzrostem kosztów badania. Kompromisem okazują się estymatory statystyki małych obszarów, które nawet przy małej liczebności próby dają dopuszczalne oszacowania wartości dla całej populacji. Podczas prezentacji zostaną przedstawione proste techniki estymacji z wykorzystaniem pakietu TeachnigSampling na przykładzie badania opinii na temat obowiązkowych praktyk studenckich na Uniwersytecie Ekonomicznym w Poznaniu.

http://www.slideshare.net/maciejberesewicz/sae-ukasz-wawrowski



Jak pobierać dane z internetu czyli parsowanie stron internetowych z pakietem XML (Maciej Beręsewicz, Katedra Statystyki UEP). Celem jest przedstawienie możliwości programu R do tak zwanego parsowania (czyli pobierania) danych ze stron internetowych. Zaprezentowane zostaną możliwości pakietu XML, jak go rozgryźć i praktyczne przykłady z rynku nieruchomości, branży muzycznej oraz sportowej.

http://www.slideshare.net/maciejberesewicz/pakiet-xml-maciej-bersewicz