# Linear algebra in R
Maciej Beręsewicz  
14 Jan 2016  



# Outline

1. Linear algebra in R -- basics
2. How to speed up calculations? 
3. Parallel processing
4. RcppArmadillo

# Linear algebra in R -- basics

R: 
* each time makes a copy of matrix (for instance see cbind, rbind)
* does not support sparse matrices
* have problems with big matrices (allocation)
* basic functions:
    + vector(), matrix(), array()
    + as.vector(), as.matrix(), as.array(),
    + diag(),
    + t(), solve(), MASS::ginv(),
    + svd(), eigen(),
    
    
# How to speed up calculations? 
    
Source: http://pj.freefaculty.org/blog/?p=122

1. Want X'X:  don't do t(X) %*% X, do crossprod(X)
2. don't do t(X) %*% y, do crossprod(X,y)
3. don't do X %*% t(y), do tcrossprod(X,y)
4. don't do crossprod(v, t(m)), do tcrossprod(v, m)


# How to speed up calculations?  - useful packages

Useful packages

* `Matrix` - package for sparse matrix manipulations
* `slam` - package for sparse matrix manipulations
* `MatrixModels` - package for modelling based on sparse matrix (glm)
* `matrixStats` - package containing several functions for matrix calculations
* `bigmatrix` and `bigalgebra` - package for matrices of moderate sizes
* `gmatrix` - package enables the evaluation of matrix and vector operations using GPU coprocessors

For instance `glmnet` allows to use sparse matrices

# How to speed up calculations?  - sparse Matrix


```r
library(Matrix)
library(slam)
```


```r
## Matrix
m1 <- matrix(0, nrow = 1000, ncol = 1000)
m2 <- Matrix(0, nrow = 1000, ncol = 1000, sparse = TRUE)
 
object.size(m1)
```

```
## 8000200 bytes
```

```r
object.size(m2)
```

```
## 5632 bytes
```


```r
## Slam
m1 <- matrix(0, nrow = 1000, ncol = 1000)
m2 <- simple_triplet_zero_matrix(nrow = 1000, ncol = 1000)
 
object.size(m1)
```

```
## 8000200 bytes
```

```r
object.size(m2)
```

```
## 1032 bytes
```


# How to speed up?

* Basic `R` do not take advantage of processors with multiple cores (multi-threading)

* However, it is possible to link R to libraries that allow for parallel processing:

    + **BLAS** -  Basic Linear Algebra Subprograms, is a specification that prescribes a set of low-level routines for performing common linear algebra operations such as vector addition, scalar multiplication, dot products, linear combinations, and matrix multiplication. 
    + **LAPACK** - Linear Algebra Package, is a standard software library for numerical linear algebra. 
    + `ATLAS` - Automatically Tuned Linear Algebra Software, an open source implementation of BLAS APIs for C and Fortran 77.
    + `Intel MKL` - The Intel Math Kernel Library, supporting x86 32-bits and 64-bits, available free from Intel.
    + `OpenBLAS` - Optimized BLAS based on GotoBLAS, supporting x86, x86-64, MIPS and ARM processors.
    + `Accelerate` (MacOS) -- Apple's framework for Mac OS X and iOS that include tuned version of BLAS and LAPACK.
    + `Eigen` - The Eigen template library provides an easy to use highly generic C++ template interface to matrix/vector operations and related algorithms like solving algorithms, decompositions etc. This library is used in `lme4` and `RcppEigen` package.

* In addition, one could also use `NVIDIA CUDA SDK`

# How to speed up? 

* R and BLAS - http://lostingeospace.blogspot.com/2012/06/r-and-hpc-blas-mpi-in-linux-environment.html
* R and MKL - http://www.flaviobarros.net/2013/06/19/compiling-r-3-0-1-with-mkl-support/
* R and OpenBLAS - http://www.lindonslog.com/linux-unix/compile-r-openblas-source-guide/
* Official manual - https://cran.r-project.org/doc/manuals/r-devel/R-admin.html#Linear-algebra
* http://www.uio.no/studier/emner/matnat/fys/FYS4411/v13/guides/installing-armadillo/


On Linux 
```
wget http://cran.r-project.org/src/base/R-3/R-3.2.3.tar.gz
tar -zxvf R-3.2.3.tar.gz 
cd R-3.2.3
./configure --enable-R-shlib --enable-BLAS-shlib --enable-threads=posix --with-lapack  --with-blas="-L/usr/local/atlas/lib -lptf77blas -lpthread -latlas" 
make
make install 
```

On Mac:

```
brew tap homebrew/science
brew install gcc
brew install Caskroom/cask/xquartz
brew install r --with-openblas

```

# How to speed up? The easy way - MRO

* Microsoft R Open (MRO), earlier Revolution R Open release enhanced distribution of open source R - https://mran.revolutionanalytics.com
* This version is already using `BLAS/LAPACK` libraries.
* This version requires installing `Intel Math Kernel Library (MKL)` for Windows and Linux, on Mac it uses `Accelerate` framework
* Version 3.2.3 will be released on 19.01.2016

```
R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

Revolution R Open 3.2.2
Default CRAN mirror snapshot taken on 2015-08-27
The enhanced R distribution from Revolution Analytics
Visit mran.revolutionanalytics.com/open for information
about additional features.

Multithreaded BLAS/LAPACK libraries detected. Using 8 cores for math algorithms.
[Previously saved workspace restored]

> 
> 
```

# How to speed up? The easy way - MRO - examples

Test 1: Computation Time

* The test environment: Five tests on matrix operations run on a Windows 8.1 64-bit; 4-core Intel core i7 laptop.
* The results: From the graphic you can see that a matrix multiplication runs 48 times faster with the MKL than without, and linear discriminant analysis is 5.5 times faster.
* Source: https://gist.github.com/j-martens/a4d2fb38ed15147f7e47#file-mkl-benchmark-rro-r

![img](https://mran.revolutionanalytics.com/assets/img/bench1.d70e631d.png)

# How to speed up? The easy way - MRO - examples

Test 2: Urbanek Benchmarks

* Another famous benchmark was published by Simon Urbanek, one of the members of R-core. You can find his code here. His benchmark consists of three different test categories:
    + Matrix calculation, 
    + Matrix functions, 
    + Programming, 
* Source: https://gist.github.com/j-martens/834b021ef69953d6ff2c#file-urbanek-benchmark-25-r

![img](https://mran.revolutionanalytics.com/assets/img/benchmark-urbanek-rro.3c7556d9.png)

# Armadillo and RcppArmadillo -- not only parallel processing

![arma](http://arma.sourceforge.net/img/armadillo_logo.png)

**Armadillo**: 

* Armadillo is a high quality C++ linear algebra library, aiming towards a good balance between speed and ease of use 
* Provides efficient classes for vectors, matrices and cubes, as well as 150+ associated functions (e.g.. contiguous and non-contiguous submatrix views) 
* Various matrix decompositions are provided through integration with LAPACK, or one of its high performance drop-in replacements (e.g.. multi-threaded Intel MKL, or AMD ACML, or OpenBLAS)  -- if available it uses 
* The library is open-source software, distributed under a license useful in both open-source and proprietary contexts 
* The Armadillo library can be distributed and/or modified under the terms of the Mozilla Public License 2.0 (MPL)
* http://arma.sourceforge.net

Last version: **Version 6.400** (Flying Spaghetti Monster Deluxe), 2015-12-15

**RcppArmadillo** -- Rcpp wrapper to Armadillo

# RcppArmadillo - examples

Source: http://gallery.rcpp.org/articles/fast-linear-model-with-armadillo/
```
// [[Rcpp::depends(RcppArmadillo)]]

#include <RcppArmadillo.h>
using namespace Rcpp;

// [[Rcpp::export]]
List fastLm(const arma::vec & y, const arma::mat & X) {

  int n = X.n_rows, k = X.n_cols;

  arma::colvec coef = arma::solve(X, y);
  arma::colvec resid = y - X*coef;

  double sig2 = arma::as_scalar(arma::trans(resid)*resid/(n-k));
  arma::colvec stderrest =
    arma::sqrt(sig2 * arma::diagvec( arma::inv(arma::trans(X)*X)) );

  return List::create(Named("coefficients") = coef,
                      Named("stderr")       = stderrest);
}
```

# RcppArmadillo - examples

Based on: http://q-aps.princeton.edu/sites/default/files/q-aps/files/slides_day4_am.pdf

1. Matrix in, Matrix out

```
// [[Rcpp::export()]]
arma::mat a1 (arma::mat x) {
  return(x) ;
}
```

2. Numeric vector in, Matrix out.

```
// [[Rcpp::export()]]
arma::mat a3 (NumericMatrix x) {
  arma::mat y = as<arma::mat>(x) ;
  return(y) ; 
}
```

3. Matrix in, numeric vector out

```
// [[Rcpp::export()]]
NumericMatrix a4 (arma::mat x) {
  NumericMatrix y = wrap(x) ;
  return(y) ; 
}
```

# RcppArmadillo - examples

4.  Linear regression

```
// [[ Rcpp :: export ()]]
arma::mat lmA (arma::mat X,
              arma::mat y) { 
  arma::mat betahat ;
  betahat = (X.t() * X ).i() * X.t() * y ;
  return(betahat) ;
}
```

# RcppArmadillo - basics - clases

* `double`, `int` ...

* `mat` - matrix (fmat -- float, mat -- double, cx_fmat -- std::complex<float>, cx_mat -- std::complex<double>, umat, imat -- integer matrix); for instance

```
mat A(5, 5, fill::randu);
```

* `vec` -- column vector (colvec -- column vector, and similar types as in `mat`)

```
vec x(10);
vec y = zeros<vec>(10);
```

* `rowvec` -- row vector (the same as in column vector)

```
rowvec x(10);
rowvec y = zeros<rowvec>(10);
```

* `cube` -- array, 3D matrix (same types as for `mat`)

```
cube x(1,2,3);
cube y = randu<cube>(4,5,6);
```

* `sp_mat` -- sparse matrix

```
sp_mat A(5,6);
sp_mat B(6,5);
```

# RcppArmadillo - basics

| (Rcpp)Armadillo | R |
|:-:|:---------------:|
| A - k  | A - k | 
| k - A  | k - A | 
| A + k  | A + k | 
| A * k  | A * k | 
| A + B  | A + B | 
| A - B  | A - B | 
| A * B  | A \%\*\% B | 
| A \% B  | A * B | 
| A / B  | A / B | 
| A == B  | all.equal(A,B) | 
| A != B  | !all.equal(A,B) | 
| A >= B  | A >=B | 
| A <= B  | A <= B | 

# RcppArmadillo - basics

| (Rcpp)Armadillo | R |
|:-:|:---------------:|
| A.n_rows | nrow(A) | 
| A.n_cols | ncol(A) | 
| A.n_elem | nrow(A)\*ncol(A) | 
| mat A; A.zeros() | A = matrix(0,nrow,ncol) | 
| mat A = zeros<mat>(k,k) | A = matrix(0,nrow,ncol) | 
| mat A; A.ones() | A = matrix(1,nrow,ncol) | 
| A++ | A = A+1 | 
| A\-\- | A = A-1 |
| X = join_rows(A,B) | X = rbind(A,B)  |
| X = join_cols(A,B) | X = cbind(A,B)  |
| X = join_cols(A,B) | X = cbind(A,B)  |
| X( span(1,2), span(3,4) )  | X[1:2,3:4] | 
| X(1,2) = 3 | x[1,2] = 3  | 
| x.diag() | diag(X) | 
| x.head() | head(X) | 
| x.tail() | tail(X) | 

# RcppArmadillo - basics

| (Rcpp)Armadillo | R |
|:-:|:---------------:|
| zeros(rows [, cols [, slices]))  | vector(n); matrix(0); array(0) |
| ones(rows [, cols [, slices]))  | vector(n); matrix(0); array(0) |
| eye(rows, cols) | diag(x = 1,nrow = 5) | 
| repmat(X, row_copies, col_copies)  | Matrix::bdiag(list) | 
| det(X) | det(X) | 
| norm(X, p) | norm(X, type) |
| rank(X) | rank(X) |  
| min(X, dim=0); max(X, dim=0) | apply(X,dim,fun); matrixStats::rowMins()
| trans(X) or X.t() | t(X) | 
| R = chol(X) | R = chol(X) | 
| inv(X) or X.i() | solve(X) | 
| pinv(X) | Matrix::ginv(X); MASS::ginv() | 
| lu(L, U, P, X) | Matrix::lu() | 
| qr(Q, R, X) | qr() | 
| X = solve(A, B) | X = solve(A, B) | 
| s = svd(X); svd(U, s, V, X) | s=svd();

# RcppArmadillo - basics using C++11

RcppArmadillo implements C++11 lambda functions (just like in R or Python)

* `each_col()`, `each_row()` - operations on columns, rows
* `each_slice()` - operations on selected columns, rows
* `for_each()` -- For each element, pass its reference to a functor or lambda function, For matrices, the processing is done column-by-column, For cubes, processing is done slice-by-slice, with each slice treated as a matrix

Example lambda in C++11


```
// C++11 only examples
mat A = ones<mat>(4,5);

// add 123 to each element
A.for_each( [](mat::elem_type& val) { val += 123.0; } );  // NOTE: the '&' is crucial!

field<mat> F(2,3);

// set the size of all matrices in field F
F.for_each( [](mat& X) { X.zeros(4,5); } );  // NOTE: the '&' is crucial!

```

# RcppArmadillo - example codes (1)

Kalibracja - jak poradzić sobie z brakami danych w badaniach częsciowych (i nie tylko)

$$
D(\boldsymbol{w},\boldsymbol{d}) = \sum_{i=1}^{n} d_iG(\frac{w_i}{d_i}) \to min,
$$

$$
\sum_{i=1}^{n} w_i x_{ij} = \boldsymbol{X}_j, \mbox{ j = 1, ..., k},
$$

$$
L \leq \frac{w_i}{d_i} \leq U, \mbox{ where $L < 1$ and $U >1$, $i=1, ..., n$}.
$$

Wybieramy postać funkcji $G(x) = \frac{1}{2}(x-1)^2$, trochę przekształceń i w wyniku otrzymujemy następującą liniową postać wektora (Szymkowiak, 2009).

$$
w_{i}=d_{i}+d_{i}\left(\textbf{X}-\hat{\textbf{X}}\right)^{T}\left(\sum_{i=1}^n{d_{i}\underline{\textbf{x}}_{i}\underline{\textbf{x}}_{i}^{T}}\right)^{-1}\underline{\textbf{x}}_{i},
$$


# RcppArmadillo - example codes (2)


RcppArmadillo - code

```
// [[Rcpp::depends(RcppArmadillo)]]

#include <RcppArmadillo.h>

using namespace Rcpp;
using namespace arma;

// [[Rcpp::export]]
NumericVector calib_linear (mat X,
                     colvec d,
                     colvec totals,
                     double eps=1e-06) {
  colvec lambda  = pinv( (X.each_col() % d).t() * X, eps) * (totals - (d.t() * X).t());
  colvec w = (1 + X * lambda) % d;
  NumericVector y = NumericVector(w.begin(),w.end());
  return (y);
}
```

# RcppArmadillo - example codes (3)

RcppArmadillo - R code

```
calib_linear_r <- function(X,d,totals,eps=1e-06) {
  lambda <- ginv(t(X * d) %*% X, tol = eps) %*% (totals - as.vector(t(d) %*% X))
  w <- (1 + as.vector(X %*% lambda))*d
  return(w)
}
```



```
> benchmark(calib_linear_r(aux, eusilc$rb050, totals),
+ calib_linear(aux, eusilc$rb050, totals))
test                    replications elapsed relative
1   calib_linear_r()        100   0.149    3.465 
2   calib_linear()          100   0.043    1.000
> 
```

# RcppArmadillo - example codes (1)

SVD example

```
mat svd_c (const mat& X) {
  mat Y = svd(X);
  return(Y);
}
```

```
set.seed (1)
m <- 1000
n <-  500
A <- matrix (runif (m*n),m,n)
benchmark(svd(A),svd_c(A))
```

```
      test replications elapsed relative 
2 svd_c(A)          100   5.872    1.000
1   svd(A)          100  14.850    2.529
```


# Literature

* Szymkowiak, Marcin (2009), Estymatory kalibracyjne w badaniu budżetów gospodarstw domowych, UE Poznań, http://www.wbc.poznan.pl/dlibra/docmetadata?id=115816
* Documentation -- http://arma.sourceforge.net/docs.html
* RcppArmadillo intro  -- https://cran.r-project.org/web/packages/RcppArmadillo/vignettes/RcppArmadillo-intro.pdf
* Armadillo intro -- http://arma.sourceforge.net/armadillo_nicta_2010.pdf 
* RcppArmadillo  in R -- http://q-aps.princeton.edu/sites/default/files/q-aps/files/slides_day4_am.pdf
* http://www.rochester.edu/college/psc/thestarlab/help/moreclus/BLAS.pdf

