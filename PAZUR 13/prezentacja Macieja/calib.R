
# libraries ---------------------------------------------------------------

library(RcppArmadillo)
library(Rcpp)
library(laeken) # data for example
library(rbenchmark)
library(microbenchmark)

# rcpparmadillo code for calibration --------------------------------------

sourceCpp('PAZUR 13/prezentacja Macieja/calib.cpp')

# r code for calibration --------------------------------------------------

calib_linear_r <- function(X,d,totals,eps=1e-06) {
  lambda <- ginv(t(X * d) %*% X, tol = eps) %*% (totals - as.vector(t(d) %*% X))
  w <- (1 + as.vector(X %*% lambda))*d
  return(w)
}


# example -----------------------------------------------------------------

data(eusilc)
# construct auxiliary 0/1 variables for genders
aux <- rbind(calibVars(eusilc$rb090),calibVars(eusilc$rb090))
totals <- c(3990798, 4191431)*2
d <- c(eusilc$rb050,eusilc$rb050)
w1 <- calib_linear_r(aux, d, totals)
w2 <- calib_linear(aux,d, totals)
all.equal(w1,w2)



# benchmark ---------------------------------------------------------------

benchmark(lin = calib_linear_r(aux, d, totals),
          rcpp= calib_linear(aux, d, totals))


