// [[Rcpp::depends(RcppArmadillo)]]

#include <RcppArmadillo.h>

using namespace Rcpp;
using namespace arma;

// [[Rcpp::export]]
NumericVector calib_linear (mat X,
                     colvec d,
                     colvec totals,
                     double eps=1e-06) {
  colvec lambda  = pinv( (X.each_col() % d).t() * X, eps) * (totals - (d.t() * X).t());
  colvec w = (1 + X * lambda) % d;
  NumericVector y = NumericVector(w.begin(),w.end());
  return (y);
}