# PAZUR 13
Maciej Beręsewicz  
13 Stycznia 2015  



## Plan spotkania

1. Wprowadzenie (kwestie organizacyjne, wiadomości ze świata R)
2. Prezentacja Alicji
3. Przerwa kawowa
4. Prezentacja Macieja
5. Część nieoficjalna

## Wprowadzenie

1. Przenosiny zapisów na portal MeetUP

*MeetUP* -- jest to portal społecznościowy, którego celem jest ułatwienie organizacji i promocji spotkań. W Polsce są trzy grupy poświęcone R: SER, eRKA oraz Wrocławskie spotkania Data Science.

2. Adres grupy: http://www.meetup.com/Poznan-R-User-Group-PAZUR/ aby dołączyć należy się zarejestrować

3. Dlaczego wybraliśmy MeetUP?
    + Microsoft, wcześniej Revolution Analytics, dofinansowuje spotkania użytkowników R oraz Data Science, a mając stronę na meetup możemy wykazać liczbę członków oraz informować o spotkaniach
    + Ułatwi organizację spotkań oraz kontakty między członkami (możliwość zadawania pytań, dyskusji nad prezentacjami)
    + Pozwoli na większą promocję grupy, ponieważ w Poznaniu jest dużo grup zajmujących się IT, które mogą być zainteresowane naszymi spotkaniami
    
## Wprowadzenie

Kolejny PAZUR: 19 luty

http://www.meetup.com/Poznan-R-User-Group-PAZUR/events/228010172/


## Nowości ze świata R - Nowy numer R-Journal

LINK: https://journal.r-project.org/archive/2015-2/

* Fitting Conditional and Simultaneous Autoregressive Spatial Models in hglm
* VSURF: An R Package for Variable Selection Using Random Forests
* zoib: An R Package for Bayesian Inference for Beta Regression and Zero/One Inflated Beta Regression
* apc: An R Package for Age-Period-Cohort Analysis
* QuantifQuantile: An R Package for Performing Quantile Regression Through Optimal Quantization
* Numerical Evaluation of the Gauss Hypergeometric Function with the hypergeo Package
* SRCS: Statistical Ranking Color Scheme for Visualizing Parameterized Multiple Pairwise Comparisons with R
* An R Package for the Panel Approach Method for Program Evaluation: pampe
* BSGS: Bayesian Sparse Group Selection
* ClustVarLV: An R Package for the Clustering of Variables Around Latent Variables
* Working with Multilabel Datasets in R: The mldr Package
* PracTools: Computations for Design of Finite Population Samples
* ALTopt: An R Package for Optimal Experimental Design of Accelerated Life Testing
* abctools: An R Package for Tuning Approximate Bayesian Computation Analyses
* mtk: A General-Purpose and Extensible R Environment for Uncertainty and Sensitivity Analyses of Numerical Experiments
* treeClust: An R Package for Tree-Based Clustering Dissimilarities
* mmpp: A Package for Calculating Similarity and Distance Metrics for Simple and Marked Point Processes
* Open-Channel Computation with R
* Generalized Hermite Distribution Modelling with the R Package hermite
* Code Profiling in R: A Review of Existing Methods and an Introduction to Package GUIProfiler

## Nowości ze świata R - The R Consortium

LINK: https://www.r-consortium.org

* The R Consortium, Inc. is a group organized under an open source governance and foundation model to provide support to the R community, the R Foundation and groups and individuals, using, maintaining and distributing R software.


Here are some of its specific aims and objectives:

* To create infrastructure and standards to benefit all the users of the R language.
* To promote the R language as a vital component of a production Data Science platform, in industry, academia, and government.
* To create and promote best practices for the development and maintenance of R language code and applications by R authors and users.
* To make it easier to use R in more environments, by developing and promoting best practices for evaluating, adopting, validating, and managing R code and applications in an end user organization.
* To support the annual useR conference as the primary conference for the R community.
* To provide information and metrics relating to the ongoing growth and adoption of the R language.

M.in. R będzie dostępny z poziomu Virtual Studio (LINK: http://blog.revolutionanalytics.com/2016/01/r-coming-to-visual-studio.html)


## Nowości ze świata R - nowe pakiety

Jak śledzić nowe pakiety? 

http://dirk.eddelbuettel.com/cranberries/index.html

![](cranberries.png)

## Nowości ze świata R - nowe pakiety

* `multidplyr` --  pakiet do przetwarzania równoległego (rozwijany m.in. przez Hadleya, https://github.com/hadley/multidplyr)

```
library(dplyr)
library(multidplyr)
library(nycflights13)
flights1 <- partition(flights, flight)
#> Initialising 7 core cluster.
flights2 <- summarise(flights1, dep_delay = mean(dep_delay, na.rm = TRUE))
flights3 <- collect(flights2)

flights2
#> Source: party_df [3,844 x 2]
#> Shards: 7 [482--603 rows]
#> 
#>    flight  dep_delay
#>     (int)      (dbl)
#> 1       1  5.2932761
#> 2      16 -0.2500000
#> 3      19 10.0500000
#> 4      28 13.6000000
#> 5      32 11.7884615
#> 6      40 12.5166667
#> 7      46  0.0000000
#> 8      49 -0.4827586
#> 9      59  3.6527197
#> 10     65  7.6979167
#> ..    ...        ...
```

## Nowości ze świata R - nowe pakiety

* Nowa wersja pakietu `ggplot2` (2.0.0), ogromna liczba zmian:
    + ggplot2 now has an official extension mechanism.
    + There are a handful of new geoms, and updates to existing geoms.
    + The default appearance has been thoroughly tweaked so most plots should look better.
    + Facets have a much richer set of labelling options.
    + The documentation has been overhauled to be more helpful, and require less integration across multiple pages.
    + A number of older and less used features have been deprecated.

* Nowa wersja pakietu `purrr` -- ułatwia przetwarzanie różnych typów obiektów

* ddR: Distributed Data Structures in R -- rozproszone dane i przetwarzanie:
    + dlist, dframe, darray
    + dmapply or dlapply 
    + https://github.com/vertica/ddR

## Nowości ze świata R - nowe pakiety

* Pakiek `ggrepel` -- ułatwia nanoszenie etykiet na wykresy ggplot2
    

```r
library(ggplot2)
ggplot(mtcars) +
  geom_point(aes(wt, mpg), color = 'red') +
  geom_text(aes(wt, mpg, label = rownames(mtcars))) +
  theme_classic(base_size = 16)
```

![](PAZUR_13_files/figure-slidy/unnamed-chunk-1-1.png)\

## Nowości ze świata R - nowe pakiety


```r
library(ggrepel)
set.seed(42)
ggplot(mtcars) +
  geom_point(aes(wt, mpg), color = 'red') +
  geom_text_repel(aes(wt, mpg, label = rownames(mtcars))) +
  theme_classic(base_size = 16)
```

![](PAZUR_13_files/figure-slidy/unnamed-chunk-2-1.png)\

## Nowości ze świata R - nowe pakiety

* https://github.com/thomasp85/ggforce - nowe geomy do ggplot2
* https://github.com/hrbrmstr/ggalt – nowe geomy i coordynaty
* https://github.com/slowkow/ggrepel – świetne etykietowanie punktów
* https://github.com/thomasp85/ggraph - pakiet do tworzenia grafów w ggplot2


## Nowości ze świata R

Nowości książkowe:

* Analiza i prognozowanie szeregów czasowych. Praktyczne wprowadzenie na podstawie środowiska R  (http://ksiegarnia.pwn.pl/Analiza-i-prognozowanie-szeregow-czasowych,129335304,p.html)
    + Wczytywanie i podstawowe operacje na danych w środowisku R,
    + Graficzna prezentacja danych,
    + Przekształcenia wstępne szeregów,
    + Dekompozycja szeregów – identyfikacja regularnych tendencji w danych,
    + Modele ARIMA,
    + Prognozowanie szeregów.
    
    
![img](http://quantup.pl/wp-content/uploads/2015/11/analiza-i-prognozowanie1.jpg)


## 

Dziękuję za uwagę!


2. Prezentacja Alicji
3. Przerwa kawowa
4. Prezentacja Macieja
5. Część nieoficjalna (Ministerstwo browaru)





