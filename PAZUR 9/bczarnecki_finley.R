library(verification)

# przyklad z finley'a

finley=matrix(nrow=2,ncol=2,c(28,23,72,2680))
wynik1=verify(finley,frcst.type="binary")

noskill=matrix(nrow=2,ncol=2,c(0,51,0,2752))
wynik2=verify(noskill,frcst.type="binary")
