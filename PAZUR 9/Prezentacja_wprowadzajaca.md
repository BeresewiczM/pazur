# PAZUR 9
SKN Estymator, Katedra Statystyki UEP, Katedra Metod Matematycznych i Statystycznych UP  
13 Feb 2015  

## Plan dzisiejszego spotkania

1. Kilka informacji wstępnych
2. Pierwszy referat - Bartosz Czernecki, Zakład Klimatologii UAM
3. Przerwa (30 minut)
4. Drugi referat - Joanna Wilk, Analyx
5. Część nieoficjalna - pub Amore del Tropico ;)

## Kilka informacji ze świata R

* IBE udostępnia API do wyników matur, egzaminu gimnazjalnego i sprawdziany z ostatnich kilku lat
http://zpd.ibe.edu.pl/.
* Vagla (prawnik walczący o dostęp do informacji publicznej i nie tylko) spowodował że powstał pakiet do orzeczeń sądowych (wszystkich orzeczeń sądowych) - https://github.com/bartekch/saos.
* Powstał i jest rozwijany pakiet **translateSPSS2R**, który umożliwia tłumaczenie składni SPSS na R - http://blog.eoda.de.
* Nowe numer Journal of Statistical Software w całości poświęcony jest analizom przestrzennym (głównie w R) i zawierać ma 22 artykuły - http://www.jstatsoft.org/v63.
* Niedawno ggplot2 otrzymał numer 1.0.0 i planowana jest również aktualizacja książki pod takim samym tytułem.

## Kilka informacji ze świata R

* Przemek Biecek stworzył ostatnio pakiet zawierający dane z Diagnozy Społecznej dostępny na github - https://github.com/pbiecek/Diagnoza
* Przemek będzie również prowadził MOOC (Massive Open Online Course) z R, który z tego co się orientuje będzie dostępny dla wszystkich za darmo - https://github.com/pbiecek/MOOC
* Hadley stworzył pakiet (pierwszą wersję) haven, który umożliwia szybkie wczytanie danych m.in. z SAS oraz tworzone jest (w końcu!) sensowne rozwiązane w nadawaniu etykiet dla wartości kolumn (coś z czego "słynie" SPSS czy SAS).
* Hadley tworzy również nową książkę o tworzeniu własnych pakietów w R. Dostępna w wersji online na stronie - http://r-pkgs.had.co.nz. Książka będzie dostępna w kwietniu.

## Kolejne PAZURy

W kwietniu przewidujemy dwa spotkania! 

* 10 kwietnia - Dr Łukasz Smaga (o analizie wariancji dla danych funkcjonalnych) oraz Dr Tomasz Górecki (Profilowanie i debugowanie kodu w R)

* 17|24 kwietnia - prezentacje wygłoszą pracownicy firmy IOKI Pearson zajmujących się analizą danych z portali edukacyjnych (nauka online)

## Zaproszenie do wygłaszania referatów!

Jeżeli ktoś z Was jest zainteresowany wygłoszeniem referatu zapraszam do kontaktu!

- Osobiście (np. podczas przerwy)
- Osobiście (np. podczas picia piwa)
- Elektronicznie - maciej.beresewicz@ue.poznan.pl lub pazur@konf.ue.poznan.pl

## Czas referatów

18:10 - 18:40 - Probabilistyczne prognozy pogody z wykorzystaniem języka programowania R, Bartosz Czernecki, Zakład Klimatologii UAM 

18:40 - 19:10 - Przewa kawowa

19:10 - 19:50 - rpy2, czyli jak przemycić R w Pythonie, Joanna Wilk, Analyx

20:00 - spotykamy się na przed wejściem do tego budynku :)

## 

Dziękuję za uwagę!


