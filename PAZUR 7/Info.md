# PAZUR 7
Serdecznie zapraszamy na kolejny, wyjątkowy, siódmy, Poznański Akademicki Zlot Użytkowników R!

9 maja 2014 r. o godz. 18.00
sala 0011A (w przyziemiu)
Uniwersytet Ekonomiczny w Poznaniu
al. Niepodległości 10
61-875 Poznań

Spotkanie sponsoruje firma Revolution Analytics

Program spotkania:
18:00 - 19:00 - prof. Roger Bivand "The R Development Process: status and prospects"
19:00 - 19:30 - przerwa
19:30 - 20:15 - dr Jarosław Jasiewicz "Spatial data mining. Zastosowania w archeologii"
20:15 - ??:?? - spotkanie integracyjne

Wydarzenie na facebook: https://www.facebook.com/events/562749743841121

Serdecznie zapraszamy wszystkich zainteresowanych. Poniżej krótki opis autorów oraz referatów:

prof. Roger Bivand - Department of Economics, Norwegian School of Economics (Bergen, Norway), Ordinary Member of "The R Foundation for Statistical Computing" and "R Core Team"

"The R Development Process: status and prospects"

R is many things to many people: a software application for getting the work done (and/or for teaching); a statistical programming language with a community of users; an ecology of contributed packages addressing the needs of different scientific communities; communities of developers with different tastes, abilities and desires; and, not least, a value generating vehicle in the software business. Many of the component parts of these roles harmonise, but many do not. The talk will use observations from the development of contributed packages for spatial data analysis in R to cast light on the current status of R development, on the management of contributed packages, and on prospects; particular stress will be placed on the problematic disproportions in the concerns of various user and developer communities with regard to the future capacity of our language and community of choice.

dr. Jarosław Jasiewicz - Instytut Geoekologii i Geoinformacji, Uniwersytet Adama Mickiewicza w Poznaniu

"Spatial data mining. Zastosowania w archeologii"

Punkty rozproszone na pewnym obszarze wskazujące występowanie jakiegoś zjawiska (np. śladów osadniczych) są niezbilansowane. Oznacza to że zbór uczący zawiera relatywnie mało przykładów pozytywnych (ślady osadnicze) i b. dużo przykładów negatywnych (brak śladów). Opracowanie klasyfikatora przestrzennego w takim wypadku jest bardzo trudne. Referat przedstawia jak przy pomocy R i wbudowanych narzędzi uczenia maszynowego (CART, SVM, Bayes, random forest itp) oraz prostych skryptów budować klasyfikatory przestrzenne radzące sobie z niezbilansowanymi danymi, nie tylko w kontekście przestrzennym.

W celu lepszej organizacji spotkania prosimy o rejestrację za pomocą poniższego formularza.