# PAZUR 12
Maciej Beręsewicz  
24 Apr 2015  

# PAZUR 12
SKN Estymator, Katedra Statystyki UEP, Katedra Metod Matematycznych i Statystycznych UP  
22.05.2015 

## Plan dzisiejszego spotkania

1. Kilka informacji wstępnych 
2. Pierwszy referat - **Jakub Nowosad**, Instytut Geoekologii i Geoinformacji, UAM
3. Grill w pubie **Fort Columb**

## Materiały z PAZURÓW

Przypominamy, że wszystkie materiały z PAZURów znajdują się na portalu **BitBucket** pod adresem:

http://bitbucket.org/BeresewiczM/pazur

**BitBucket** jest podobną platformą, jak **GitHub** (oparty na systemie wersjonowania kodu *git*).

## Nowości w świecie R

* 18 maja ukazała się nowa wersja oprogramowania **Revolution R Enterprise 7.4**, wersji **R** przeznaczonej dla przedsiębiorstw. Zawiera m.in:
    + nową implementację klasyfikatora Naive Bayes,
    + optymalizację pracy z "szerokimi" danymi
    + obsługę HDFS na Hadoop
    
* ukazała się nowa wersja pakietu **devtools** (1.8.0) ułatwiającego tworzenie pakietów
* 5 maja ukazał się pakiet **stringr** w wersji 1.0.0., który jest Hadley'ową wersją pakietu **stringi** stworzonego przez Marka Gągolewskiego (de facto opartą o ten pakiet).
* **scholar** - umożliwiający import danych (cytowań, tytułów artykułów) z Google Scholar.

## Nowości w świecie R

* powstał pakiet **DiagrammeR**, który umożliwia tworzenie diagramów oraz schematów (zbliżonych do tych z TeX i Tikz).

<iframe src="http://cran.r-project.org/web/packages/DiagrammeR/vignettes/DiagrammeR.html" height="800" align="middle"></iframe>

## Nowości w świecie R

Pogromcy Danych część 2!

Wizualizacja i modelowanie

* 6 V – 27 V
 
* Wizualizacja – jak zrobić wykres (3 części)
* Wizualizacja – co czyni wykres dobrym a co złym (3 części)
* Modelowanie danych ilościowych (4 części)
* Modelowanie danych jakościowych (3 części)
* Dane wokół nas (3 części)


## Nowości w świecie R - Open Poland


OpenPoland jest usługą, która umożliwia dostęp do aktualnych danych publicznych poprzez interfejs API. Obecnie system zawiera na bieżąco aktualizowane dane z Banku Danych Lokalnych GUS, aczkolwiek niedługo zostanie zasilony innymi źródłami. 

<iframe src="http://openpoland.net/welcome/" height="400" align="middle"></iframe>


## Nowości w świecie R - Open Poland

Pakiet **openPoland** - https://github.com/kalimu/openPoland

<iframe src="http://www.wais.kamil.rzeszow.pl/openpoland/" height="400" align="middle"></iframe>

## BlogRoll

Interesujące blogi o R tworzone w Polsce:

* [SmarterPoland](http://smarterpoland.pl)
* [Analyse-R](http://analyse-r.blogspot.co.at)
* [Adolfo Alvarez](http://adolfoalvarez.cl)
* [QuantUp Blog](http://quantup.pl/blog/) - opracowania firmy QuantUp
* [Niepewne sondaże](http://niepewnesondaze.blogspot.com) - blog poświęcony preferencjom wyborczym (modelowanie bayesowskie z R)

## Zaproszenie do wygłaszania referatów!

Jeżeli ktoś z Was jest zainteresowany wygłoszeniem referatu zapraszam do kontaktu!

- Osobiście (np. podczas spotkań)
- Elektronicznie - maciej.beresewicz@ue.poznan.pl lub pazur@konf.ue.poznan.pl

## Oferty pracy

Firma Analyx poszukuje osób ze znajomością R !

* [Data Miner / Statistical Modeler](https://dl.dropboxusercontent.com/u/39076434/Data%20Miner_Statistical%20Modeler.pdf)
* [Statistics Specialist/Developer](https://dl.dropboxusercontent.com/u/39076434/Statistics%20Specialist_Developer.pdf)
* [Junior Analyst](https://dl.dropboxusercontent.com/u/39076434/Junior%20Analyst.pdf)

Więcej szczegółów - Adolfo Alvarez :)

## Czas referatów

* 18:00 - 18:10 - Podsumowanie nowości w świecie R
* 18:10 - 19:00 - 
* 19:00 - grill w pubie **Fort Columb**

## 

Dziękuję za uwagę!


