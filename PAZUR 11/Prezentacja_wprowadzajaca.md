# PAZUR 11
Maciej Beręsewicz  
24 Apr 2015  

# PAZUR 11
SKN Estymator, Katedra Statystyki UEP, Katedra Metod Matematycznych i Statystycznych UP  
24.04.2015 

## Plan dzisiejszego spotkania

1. Kilka informacji wstępnych 
2. Pierwszy referat - **Paweł Kleka**,  Zakład Podstaw Badań Psychologicznych Instytutu Psychologii UAM
3. Przerwa (40/50 minut)
4. Drugi referat - **Bogucki Mikołaj, Jędrzejewski Krzysztof**, Pearson IOKI
5. Część nieoficjalna -- sponsorowana przez **Revolution Analytics**

## Kilka informacji ze świata R

* Kurs **Pogromcy Danych** część pierwsza wprowadzająca do R i przetwarzania danych została przedłużona do **27 maja**. 
* Na GitHub (niedługo również na CRAN) dostępny jest pakiet **tidyjson** autorstwa **Jeremy'ego Stanley'a**, który umożliwia pracę z plikami .json na podobnej zasadzie jak w **dplyr** czy **tidyr**. Przykład użycia:


```
## [
##   {
##     "sha": "c7437518aaf4c5b1105e0f4b2dce1b1672970efb",
##     "commit": {
##       "author": {
##         "name": "Romain Francois",
##         "email": "romain@r-enthusiasts.com",
##         "date": "2014-09-13T10:10:36Z"
##       },
##       "committer": {
##         "name": "Romain Francois",
##         "email": "rom
```

## Kilka informacji ze świata R


```r
library(tidyjson)
library(dplyr)
commits %>%   # single json document of github commits from dplyr
  as.tbl_json(.) %>%  # turn into a 'tbl_json'
  gather_array(.) %>%  # stack as an array
  spread_values(.,
    sha         = jstring("sha"),
    author      = jstring("commit", "author", "name"),
    author.date = jstring("commit", "author", "date")
  ) %>%
  tbl_df(.)
```

```
## Source: local data frame [30 x 5]
## 
##    document.id array.index                                      sha
## 1            1           1 c7437518aaf4c5b1105e0f4b2dce1b1672970efb
## 2            1           2 ea1563fcf2ebe08eb3b83c13040e04fa61f077e9
## 3            1           3 4abf4e1b9a1e6763accc096b1ef6ad71399747d6
## 4            1           4 f46d3a4f6385dc0b5213ee63180a6cf17c4d5d9a
## 5            1           5 e822e7a51e49cca36270bc751fa9b244fa79510d
## 6            1           6 9cf29da889f7ac0d49b4628d0195e590fabc2749
## 7            1           7 977c2f23cd6d25f313ee106beb8788bec31e8da1
## 8            1           8 c0d712ce2c36c14a8af95fdc0180a78f8b8b3f7f
## 9            1           9 ae2e8b39537c85458236b56c3c394c23d11af86a
## 10           1          10 fd00ad26293ea1415e76858e09c169d3788ad400
## ..         ...         ...                                      ...
## Variables not shown: author (chr), author.date (chr)
```

## Kilka informacji ze świata R

* W zaakceptowanych artykułach do **The R Journal** można znaleźć opis nowego pakietu **Frames2** implementującego estymację opartą na dwóch badaniach losowych (o nieprostych schematach losowania). Autorami pakietu są **Antonio Acros, David Molina, Maria Giovanna Ranalli i Maria del Mar Rueda**.
* Pakiet **sae2** dla modeli szeregów czasowych statystyki małych obszarów. Autorami są **Robert E. Fay, Mamadou Diallo**.

## Kilka informacji ze świata R

* Na GitHub jakiś czas temu powstała grupa **rOpenGov: R Ecosystem for Open Government Data and Computational Social Science**, która ma na celu promowanie dostępu do danych publicznych oraz otwartość danych. Znajdziemy tam takie pakiety jak:
    + eurostat -- umożliwia pobieranie danych z baz danych Eurostat
    + replicaX -- pakiet umożliwiający anonimizacje danych

## Kilka informacji ze świata R

* Pod koniec 2014 roku powstał ciekawy pakiet **countrycode** umożliwiający konwersję różnych kodów nazw krajów ("cowc", "cown", "iso3c", "iso3n", "iso2c", "imf", "fips104", "fao", "ioc", "un", "wb", "country.name"). Przykład użycia


```r
library(countrycode)
countrycode_data %>% tbl_df() %>% head(.,n=4) 
```

```
## Source: local data frame [4 x 15]
## 
##    country.name cowc cown fao fips104 imf ioc iso2c iso3c iso3n  un  wb
## 1   Afghanistan  AFG  700   2      AF 512 AFG    AF   AFG     4   4 AFG
## 2 Aland Islands   NA   NA  NA      NA  NA  NA    AX   ALA   248 248 ALA
## 3       Albania  ALB  339   3      AL 914 ALB    AL   ALB     8   8 ALB
## 4       Algeria  ALG  615   4      AG 612 ALG    DZ   DZA    12  12 DZA
## Variables not shown: regex (chr), continent (chr), region (chr)
```


## Zaproszenie do wygłaszania referatów!

Jeżeli ktoś z Was jest zainteresowany wygłoszeniem referatu zapraszam do kontaktu!

- Osobiście (np. podczas przerwy)
- Elektronicznie - maciej.beresewicz@ue.poznan.pl lub pazur@konf.ue.poznan.pl

## Czas referatów

* 18:00 - 18:10 - Podsumowanie nowości w świecie R
* 18:10 - 18:40 - Skracanie kwestionariuszy psychologicznych w oparciu o IRT (Paweł Kleka)
* 18:40 - 19:30 - Kawa i poczęstunek
* 19:30 - 20:00 - Analiza trudności testów sprawdzających znajomość języka angielskiego (Bogucki Mikołaj, Jędrzejewski Krzysztof)
* 20:00 - część nieoficjalna (pub Fermentowania) 

## 

Dziękuję za uwagę!


